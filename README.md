The below pointers could be helpful while reading and writing about the papers, especially while doing literature survey, attending talks, conferences.

**Agile for Research**
- https://www.nature.com/articles/d41586-019-01184-9

**Paper reading:**

- Tips on reading a systems paper: https://www.cs.princeton.edu/courses/archive/fall19/cos418/papers/reading.pdf
- 8 things you should know about a paper: https://www.cs.princeton.edu/courses/archive/fall19/cos418/518_paper_prep.txt
- How to read a paper: http://ccr.sigcomm.org/online/files/p83-keshavA.pdf
- Doing a systems PhD: https://www.cl.cam.ac.uk/research/srg/netos/eurosys11dw/keynote/StevenHand.pdf
- Writing reviews for systems conferences: https://people.inf.ethz.ch/troscoe/pubs/review-writing.pdf


**Papers Finder:**
- http://www.arxiv-sanity.com/
- https://www.connectedpapers.com/
- https://www.litmaps.co/

**Research statement:**
- Startup documents: https://drive.google.com/file/d/12hD6LbiQ3L49muAkdU1Hjkek7mADmAKb/view
- How to write a great research paper? : https://www.microsoft.com/en-us/research/academic-program/write-great-research-paper/

**Research paper life cycle:**
- How to write a great research proposal?
     https://www.microsoft.com/en-us/research/academic-program/how-to-write-a-great-research-proposal/


**Writing tips:**

- https://people.eecs.berkeley.edu/~pattrsn/talks/writingtips.html
- https://www.approximatelycorrect.com/2018/01/29/heuristics-technical-scientific-writing-machine-learning-perspective/
- [How to Write a Paper in a Weekend (By Prof. Pete Carr)](https://www.youtube.com/watch?v=UY7sVKJPTMA)
- [Writing Abstarct of peer reviewed journal paper](https://steemit.com/abstract/@noppi/writing-abstract-of-your-peer-reviewed-journal-paper)
- [Write a Research Poster](https://www.youtube.com/watch?v=1RwJbhkCA58)
- [Write a Research Abstract](https://www.youtube.com/watch?v=U__GuP8--X4)
- [Writing resources](https://www.oacommunity.org/resources)

- Inspired by Brighten Godfrey's blog post about paper rejections: https://youinfinitesnake.blogspot.com/2014/02/on-getting-rejected.html, Ankit Singla made this graphic about the lifetime of his projects/papers. He says, "Each line here is one research project, from the idea conception through typically several rejections, to some reaching publication."
- [Writing Technical Articles](https://www.cs.columbia.edu/~hgs/etc/writing-style.html)

**Graduate school research life/Advise:**

- CS298 Research Culture And Community Norms : https://inst.eecs.berkeley.edu/~cs298-7/fa20/lectures/
- Survey: Developing an Ideal group for systems research: https://docs.google.com/spreadsheets/d/1VqMDZFO2c7mEsLPaylw3da1eRNXA42a7V1f0Q_v-8Vg/edit#gid=776550912
- Effective tools for computer systems research --> https://nymity.ch/book/
- You and your research: https://www.cs.virginia.edu/~robins/YouAndYourResearch.html[YouTube Link: https://www.youtube.com/watch?v=a1zDuOPkMSw]
- https://www.csee.umbc.edu/~mariedj/papers/advice.pdf
- https://svr-sk818-web.cl.cam.ac.uk/keshav/wiki/index.php/Hints_on_doing_research
- How to do great research, Nick Feamster blog: https://greatresearch.org/
- Biswa’s posts: https://medium.com/@__biswa/two-cents-on-computer-architecture-research-103-gollus-computer-architecture-exploration-605466a0cd09
- How to have a bad graduate career: http://www.taoli.ece.ufl.edu/BadCareer.pdf
- Biswa’s tips: https://www.cse.iitk.ac.in/users/biswap/tips.html
- Non-Technical Talks by David Patterson, U.C. Berkeley:https://people.eecs.berkeley.edu/~pattrsn/talks/nontech.html
- https://muratbuffalo.blogspot.com/2013/04/my-advice-to-my-students.html



**Talks:**
- How to give a research talk? : https://www.microsoft.com/en-us/research/academic-program/give-great-research-talk/

- https://www.microsoft.com/en-us/research/publication/how-to-give-a-good-research-talk/
- How to write a conference talk:  http://www.pl-enthusiast.net/2019/01/02/how-to-write-a-conference-talk/


**Books for grads:**

- How to get control of your time and your life
- Writing for computer science
- Elements of style on writing

- Interesting book review: "Teaching While Introverted" about the book "Geeky Pedagogy: A Guide for Intellectuals, Introverts, and Nerds Who Want to Be Effective Teachers"https://www.chronicle.com/article/Teaching-While-Introverted/247963. BTW, reading the Chronicle of Higher Education is often informative about academic life

**List of Summer Schools**
- https://github.com/sshkhr/awesome-mlss
- http://mlss.cc/


**Tips to apply for Summer School**

- https://tmlss.ro/faq.php 


**Misc articles:**

- Rand Bush talk: https://interred.wordpress.com/2007/01/17/randy-bush-200245-rhodes-university/




**Courtesy:** Dr. Praveen T, Assistant Professor in Dept. of Computer Science, IIT Hyderabad.
